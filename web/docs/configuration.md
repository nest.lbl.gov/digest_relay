# The `digest_relay` Command #

The `digest_relay` executable is designed to GET a Digest from one URL and, after applying any requested transformation, POST it to another.

## Environmental Variables ##

As well as a command line tool, the `digest_relay` executable is designed to be executed in an [OSI container](https://opencontainers.org/) or similar deployment environment. To facilitate this, the `drip_feed` executable can be configured with environment variables. However, the value of these variables will overridden by anything specified in the command used to execute the container. The following are the list of valid environment variables.

* `DIGEST_RELAY_INI_FILE`
* `DIGEST_RELAY_INI_SECTION`
* `DIGEST_RELAY_LOG_FILE`
* `DIGEST_RELAY_LOG_LEVEL`
* `DIGEST_RELAY_INTERVAL`
* `DIGEST_GET_URL`
* `DIGEST_POST_URL`
* `DIGEST_SAVE_FILE`
* `DIGEST_DEPTH`
* `DIGEST_START`
* `DIGEST_SUBJECT`
