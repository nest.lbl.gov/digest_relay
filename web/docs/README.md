[digest_relay](https://gitlab.com/nest.lbl.gov/digest_relay) contains the python package that provides a utility to to GET a Digest from one URL and, after applying any requested transformation, POST it to another.

Full details for configuring the command can be found [here](web/docs/configuration.md).
