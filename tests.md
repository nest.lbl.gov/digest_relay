# Tests

The following are commands to run simple tests of the executables

## File Dropper

    drip_service -i resources/test/sentry_actions/drip/file_dropper.ini -s file_dropper.test file_drip
    drip_service -i resources/test/sentry_actions/drip/file_dropper.ini -s file_dropper.test -c reload
    drip_service -i resources/test/sentry_actions/drip/file_dropper.ini -s file_dropper.test -c stop

