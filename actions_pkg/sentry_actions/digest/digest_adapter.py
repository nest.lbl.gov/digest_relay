"""
Adapter different types of Digest to by use in DigestAction.
"""

from typing import Any, Callable, Dict, Optional

import xml.etree.ElementTree as ET

from .digest_exception import DigestException


class DigestAdapter:
    """
    This class is used by DigestAction to handle different types of digests.
    """

    def __init__(self, params: Optional[Dict[str, Any]]):
        """
        Creates an instance of this class.
        """
        self.__params = params

    def build_url(self, start: Optional[str]) -> str:
        """
        Build the URL from which to retrieve the digest.
        """
        raise DigestException('"build_url" not overloaded')

    def create_acquirable(
        self,
        get_segment: Callable[[Optional[str]], ET.Element],
        save_file: Optional[str],
        start: Optional[str],
    ):
        """
        Creates a AcquirableDigest subclass to use.
        """
        raise DigestException('"create_acquirable" not overloaded')

    def _get_params(self) -> Optional[Dict[str, Any]]:
        """
        Returns the dictionary contains all of the parameters for this task..
        """
        return self.__params
