"""
An implementation of a sentry task that GETs a Digest from one URL and
then executes an action on that Digest.
"""

from typing import Callable, Optional

import xml.etree.ElementTree as ET

from .digest_adapter import DigestAdapter
from .digest_action import GET_URL_KEY
from .ordinal_digest import OrdinalDigest


def _increase_ordinal(current: str) -> str:
    """
    Increases the supplied ordinal by one.
    """
    return str(int(current) + 1)


class OrdinalAdapter(DigestAdapter):
    """
    This class is used by DigestAction to handle ordered digests.
    """

    def build_url(self, start: Optional[str]) -> str:
        """
        Build the URL from which to retrieve the digest.
        """
        params = self._get_params()
        if None is params:
            raise ValueError("No parameters have been set")
        template_url = params[GET_URL_KEY]
        effective_url = template_url.format(ordinal=start)
        return effective_url

    def create_acquirable(
        self,
        get_segment: Callable[[Optional[str]], ET.Element],
        save_file: Optional[str],
        start: Optional[str],
    ):
        """
        Creates a OrdinalDigest as the AcquirableDigest to use.
        """
        return OrdinalDigest(
            _increase_ordinal,
            get_segment,
            save_file,
            start,
        )
