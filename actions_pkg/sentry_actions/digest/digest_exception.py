"""
Exceptions thrown by this package.
"""


class DigestException(Exception):
    """
    Base exception for all custom exception in this package
    """
