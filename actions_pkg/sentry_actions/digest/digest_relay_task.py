"""
An implementation of a 'sentry' Task that relays a Digest from
one URL to another,
"""

from typing import Any, Dict, List, Optional

import argparse
import os

import logging
import xml.dom.minidom
import xml.etree.ElementTree as ET

import requests

from ..action_task import (
    CONFIG_FILE_KEY,
    CONFIG_SECTION_KEY,
    LOG_FILE_KEY,
    LOG_LEVEL_KEY,
    INTERVAL_KEY,
)

from . import (
    GET_URL_KEY,
    SAVE_FILE_KEY,
    DEPTH_KEY,
    START_KEY,
    DigestAction,
)

POST_URL_KEY = "post_url"
SUBJECT_KEY = "subject"

_HEADERS = {"Content-Type": "application/xml", "Accept": "application/xml"}
_ENCODING = "utf-8"


class DigestRelayTask(DigestAction):
    """
    This class relays a Digest from one URL to another.
    """

    def create_argument_parser(
        self, include_interval: bool = True
    ) -> argparse.ArgumentParser:
        """
        Creates and populated the argparse.ArgumentParser for this executable.
        """
        parser = super().create_argument_parser(include_interval)
        parser.add_argument(
            f"--{POST_URL_KEY}",
            dest="POST_URL",
            help="The URL to which to POST the Digest",
        )
        parser.add_argument(
            f"--{SUBJECT_KEY}",
            dest="SUBJECT",
            help="The replacement subject, if any, to put into the Digest",
        )
        return parser

    def _digest_action(
        self,
        digest: ET.Element,
        session: requests.Session,
        params: Optional[Dict[str, Any]],
    ) -> Optional[str]:
        """
        Changes the subject of the Digest, if requested, and POST the
        modified Digest to the `pust_url`.

        Returns:
            None if the is no problem, otherwise a string explaining why
            the action could not be completed.
        """
        if None is params:
            raise ValueError("No parameters have been set")
        replacement_subject = params[SUBJECT_KEY]
        if None is not replacement_subject:
            subject_element = digest.find("subject")
            if None is subject_element:
                raise ValueError("Failed, subject element missing")
            subject_element.text = replacement_subject
        post_url = params[POST_URL_KEY]
        logging.debug("POST URL: %s", params[POST_URL_KEY])
        if "DUMMY_RELAY" in os.environ:
            logging.info(xml.dom.minidom.parseString(ET.tostring(digest)).toprettyxml())
            status = 200
        else:
            response = session.post(
                post_url, data=ET.tostring(digest), headers=_HEADERS
            )
            status = response.status_code
        if 200 != status:
            return f"Failed to post to destination, status code {status}"
        return None

    def get_defaults(self) -> Dict[str, Optional[str]]:
        """
        Return a dictionary containing any default values for parameters.
        """
        return {
            CONFIG_FILE_KEY: "${HOME}/digest_relay.ini",
            CONFIG_SECTION_KEY: "digest_relay",
            LOG_FILE_KEY: None,
            LOG_LEVEL_KEY: "INFO",
            SAVE_FILE_KEY: "${HOME}/.digest_relay",
            DEPTH_KEY: "32",
            INTERVAL_KEY: "-1",
        }

    def get_envar_mapping(self) -> Dict[str, str]:
        """
        Return a dictionary to map environmental variables to option variables.
        """
        return {
            "DIGEST_RELAY_CONFIG_FILE": "CONFIG_FILE",
            "DIGEST_RELAY_CONFIG_SECTION": "CONFIG_SECTION",
            "DIGEST_RELAY_LOG_FILE": "LOG_FILE",
            "DIGEST_RELAY_LOG_LEVEL": "LOG_LEVEL",
            "DIGEST_RELAY_INTERVAL": "INTERVAL",
            "DIGEST_RELAY_GET_URL": "GET_URL",
            "DIGEST_RELAY_POST_URL": "POST_URL",
            "DIGEST_RELAY_SAVE_FILE": "SAVE_FILE",
            "DIGEST_RELAY_DEPTH": "DEPTH",
            "DIGEST_RELAY_START": "START",
            "DIGEST_RELAY_SUBJECT": "SUBJECT",
        }

    def get_param_names(self) -> List[str]:
        """
        Returns a List of all the config items in which this instance is interested.
        """
        return [
            LOG_FILE_KEY,
            LOG_LEVEL_KEY,
            INTERVAL_KEY,
            GET_URL_KEY,
            POST_URL_KEY,
            SAVE_FILE_KEY,
            DEPTH_KEY,
            START_KEY,
            SUBJECT_KEY,
        ]

    def set_params(  # pylint: disable=useless-return
        self, params: Dict[str, Any]
    ) -> Optional[str]:
        """
        Sets a new set of parameter values.

        Returns:
            None is task configured successfully, otherwise it return
            the reason why configuration failed.
        """
        for key in [POST_URL_KEY]:
            if None is params[key]:
                return f'Option "{key}" not specified, nor provided as an environmental variable'
        return super().set_params(params)


def main():
    """
    Runs a one-of execution of `digest_relay`.
    """


if __name__ == "__main__":
    main()
