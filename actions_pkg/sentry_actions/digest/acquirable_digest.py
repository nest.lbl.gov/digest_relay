"""
Segments a long digest object into a set of smaller digests that are
returned one at a time.
"""

import xml.etree.ElementTree as ET

from .digest_exception import DigestException


class AcquirableDigest:
    """
    Provides access to a series of Digest on which to act.
    """

    def acquire(self) -> ET.Element:
        """
        Returns the next Digest on which to act.
        """
        raise DigestException('"acquire" not overloaded')

    def release(self) -> None:
        """
        Signals that current Digest has been successfully used and can be
        disposed of.
        """
        raise DigestException('"release" not overloaded')

    def rewind(self) -> ET.Element:
        """
        Signals that the current Digest has not been successfully used so
        returns the original one again.
        """
        raise DigestException('"rewind" not overloaded')
