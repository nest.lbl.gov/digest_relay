"""
Segments a long digest object that is ordered by some criteria, so that the
"next" segment can be acquired after the "current" segment is released.
"""

from typing import Callable, List, Optional, Tuple

import os
import xml.etree.ElementTree as ET

from filelock import FileLock

from .digest_exception import DigestException
from .acquirable_digest import AcquirableDigest

_ENCODING = "utf-8"
# Wait five minutes to get start time
_WATCHER_TIMEOUT = 300


def _get_seen(segment: ET.Element) -> Tuple[str, Optional[List[str]]]:
    """
    Return a list if items whose time matches the end time of the
    segment.
    """
    end_element = segment.find("end")
    if None is end_element or None is end_element.text:
        raise ValueError('Failed, "end" element missing')
    if None is end_element.text:
        raise ValueError('Failed, "end" element empty')

    tail_elements = segment.findall(f"issue/issued[time='{end_element.text}']/item")
    if 0 == len(tail_elements):
        return end_element.text, None
    tail = []
    for element in tail_elements:
        if None is not element.text:
            tail.append(element.text)
    return end_element.text, tail


def _read_start(save_file: str) -> Tuple[Optional[str], Optional[List[str]]]:
    """
    Read the start time from its persistent store.
    """
    resolved_path = os.path.expandvars(save_file)
    lock = FileLock(f"{resolved_path}.lock", timeout=_WATCHER_TIMEOUT)
    with lock:
        if os.path.exists(resolved_path):
            with open(resolved_path, "r", encoding=_ENCODING) as opened_file:
                persisted_start = opened_file.readline()[:-1]
                parts = persisted_start.split(" ", 1)
                if 1 < len(parts):
                    seen = parts[1].strip("']['").split("', '")
                    return parts[0], seen
                return parts[0], None
    return None, None


def _write_start(
    save_file: str, begin_time: str, seen_items: Optional[List[str]] = None
) -> None:
    """
    Write the start time to its persistent store.
    """
    resolved_path = os.path.expandvars(save_file)
    lock = FileLock(f"{resolved_path}.lock", timeout=_WATCHER_TIMEOUT)
    with lock:
        tmp_file = resolved_path + ".tmp"
        with open(tmp_file, "w", encoding=_ENCODING) as opened_file:
            if None is seen_items or 0 == len(seen_items):
                persisted_start = f"{begin_time}"
            else:
                persisted_start = f"{begin_time} {seen_items}"
            opened_file.write(persisted_start + "\n")
        os.replace(tmp_file, resolved_path)


class SegmentedDigest(AcquirableDigest):
    """
    Segments a long digest object into a set of smaller digests that are
    returned one at a time.
    """

    def __init__(
        self,
        get_segment: Callable[[Optional[str]], ET.Element],
        save_file: Optional[str] = None,
        begin_time: Optional[str] = None,
    ):
        self.__current: Optional[ET.Element] = None
        self.__get_segment = get_segment
        self.__save_file = save_file
        if None is not save_file:
            if None is not begin_time:
                _write_start(save_file, begin_time)
            self.__begin_time, self.__seen = _read_start(save_file)
        else:
            self.__begin_time = begin_time
            self.__seen = None

    def acquire(self) -> ET.Element:
        """
        Returns the next segment.
        """
        self.__current = self.__get_segment(self.__begin_time)

        if None is self.__seen or 0 == len(self.__seen):
            return self.__current

        # Removed any elements from the end of the previous segment
        issue_element = self.__current.find("issue")
        if None is issue_element or 0 == len(issue_element):
            return self.__current
        head_elements = issue_element.findall(f"issued[time='{self.__begin_time}']")
        if 0 == len(head_elements):
            return self.__current

        for element in head_elements:
            item_element = element.find("item")
            if (
                None is not item_element
                and None is not item_element.text
                and item_element.text in self.__seen
            ):
                issue_element.remove(element)
        return self.__current

    def release(self) -> None:
        """
        Signals that current segment has been successfully used and can
        be disposed of.
        """
        if None is self.__current:
            raise DigestException("Should not get here")
        self.__begin_time, self.__seen = _get_seen(self.__current)
        if None is not self.__save_file:
            _write_start(self.__save_file, self.__begin_time, self.__seen)

    def rewind(self) -> ET.Element:
        """
        Signals that the current segment has not been successfully used
        so returns the original one again.
        """
        if None is self.__current:
            raise DigestException("Should not get here")
        return self.__current
