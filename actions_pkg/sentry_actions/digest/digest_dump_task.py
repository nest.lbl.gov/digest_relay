"""
An implementation of the "digest_action" that simple output
the contents of the digest.
"""

from typing import Any, Dict, Optional

import logging
import xml.etree.ElementTree as ET

import requests

from ..action_task import (
    CONFIG_FILE_KEY,
    CONFIG_SECTION_KEY,
    LOG_FILE_KEY,
    LOG_LEVEL_KEY,
    INTERVAL_KEY,
)

from . import (
    SAVE_FILE_KEY,
    DEPTH_KEY,
    DigestAction,
)


class DigestDumpTask(DigestAction):
    """
    This class dump the Digest to the logging output.
    """

    def _digest_action(
        self, digest: ET.Element, _: requests.Session, __: Optional[Dict[str, Any]]
    ) -> Optional[str]:
        """
        Dumps the Digest to the logging output.

        Returns:
            None if the is no problem, otherwise a string explaining why
            the action could not be completed.
        """
        item_elements = digest.findall("issue/issued")
        if None is item_elements or 0 == len(item_elements):
            return None

        begin_element = digest.find("begin")
        if None is not begin_element and None is not begin_element.text:
            logging.info("Digest starting at %s", begin_element.text)

        for element in item_elements:
            item_element = element.find("item")
            if None is not item_element and None is not item_element.text:
                datetime_element = element.find("time")
                if None is not datetime_element and None is not datetime_element.text:
                    logging.info("%s : %s", item_element.text, datetime_element.text)
        return None

    def get_defaults(self) -> Dict[str, Optional[str]]:
        """
        Return a dictionary containing any default values for parameters.
        """
        return {
            CONFIG_FILE_KEY: "${HOME}/digest_dump.ini",
            CONFIG_SECTION_KEY: "digest_dump",
            LOG_FILE_KEY: None,
            LOG_LEVEL_KEY: "INFO",
            SAVE_FILE_KEY: "${HOME}/.digest_dump",
            DEPTH_KEY: "32",
            INTERVAL_KEY: "-1",
        }
