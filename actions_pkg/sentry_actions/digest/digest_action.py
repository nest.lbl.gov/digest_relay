"""
An implementation of a sentry task that GETs a Digest from one URL and
then executes an action on that Digest.
"""

from typing import Any, Dict, List, Optional, Type

import argparse
import os

from importlib.metadata import entry_points
import logging
import xml.etree.ElementTree as ET

import requests

from .. import (
    ActionTask,
    CONFIG_FILE_KEY,
    CONFIG_SECTION_KEY,
    LOG_FILE_KEY,
    LOG_LEVEL_KEY,
    INTERVAL_KEY,
    required_options,
)

from .digest_exception import DigestException

from .digest_adapter import DigestAdapter
from .acquirable_digest import AcquirableDigest

ADAPTER_KEY = "adapter"
GET_URL_KEY = "get_url"
SAVE_FILE_KEY = "save"
START_KEY = "begin"
DEPTH_KEY = (
    "depth"  # this is included form the convenience of not having to redefine the whole
)
# interface in segmented_action for one argument. I'm lazy.


_HEADERS = {"Content-Type": "application/xml", "Accept": "application/xml"}


def select_digest_adapter(group: str, name: str) -> Type[DigestAdapter]:
    """
    Returns the selected service class object.
    """
    services = entry_points(group=group)
    for entry in services:
        if name == entry.name:
            return entry.load()
    raise DigestException(f'No known DigestAdapter implementation named "{name}"')


class DigestAction(ActionTask):
    """
    This class GETs a Digest from one URL and then executes an action on
    that Digest.
    """

    def __init__(self) -> None:
        """
        Creates an instance of this class.
        """
        super().__init__()
        self.__acquirable_digest: Optional[AcquirableDigest] = None
        self.__adapter: Optional[DigestAdapter] = None
        self.__session = requests.Session()
        self.__session.verify = True
        self.__start: Optional[str] = None

    def create_argument_parser(
        self, include_interval: bool = True
    ) -> argparse.ArgumentParser:
        """
        Creates and populated the argparse.ArgumentParser for this executable.
        """
        parser = super().create_argument_parser(include_interval)
        parser.add_argument(
            "-a",
            f"--{ADAPTER_KEY}",
            dest="ADAPTER",
            help='The name of the DigestAdapter implementation to use. "segmented" is the default',
        )
        parser.add_argument(
            "-g",
            f"--{GET_URL_KEY}",
            dest="GET_URL",
            help="The URL from which to GET the Digest",
        )
        parser.add_argument(
            "-d",
            f"--{DEPTH_KEY}",
            dest="DEPTH",
            help="The maximum number of Items allowed in the Digest",
        )
        parser.add_argument(
            f"--{SAVE_FILE_KEY}",
            dest="SAVE_FILE",
            help="The file into which to save the action state, "
            + ' the default is "'
            + str(self.get_defaults()[SAVE_FILE_KEY])
            + '"',
        )
        parser.add_argument(
            "-b",
            f"--{START_KEY}",
            dest="START",
            help=(
                "The value to use to begin fetching digests. This will replace any saved value."
                " For time based digests this should be the ISO string for the time at or after"
                " which to get Items in the Digest"
            ),
        )
        return parser

    def _digest_action(
        self,
        digest: ET.Element,
        session: requests.Session,
        params: Optional[Dict[str, Any]],
    ) -> Optional[str]:
        """
        Executes an action using the supplied Digest.

        Returns:
            None if the is no problem, otherwise a string explaining why
            the action could not be completed.
        """
        raise DigestException('"_digest_action" not overloaded')

    def execute(self) -> bool:
        """
        Executes the responsibilities of this executable
        """
        params = self.get_params()
        if None is params:
            save_file: Optional[str] = None
        else:
            save_file = params[SAVE_FILE_KEY]
        if None is self.__adapter:
            adapter_class = select_digest_adapter("sentry", params[ADAPTER_KEY])
            self.__adapter = adapter_class(params)
        if None is self.__acquirable_digest:
            self.__acquirable_digest = self.__adapter.create_acquirable(
                self.get_digest, save_file, self.__start
            )

        error_message = self._digest_action(
            self.__acquirable_digest.acquire(), self.__session, params
        )
        if None is not error_message:
            logging.error(
                'Could not complete action with digest beacuse"%s"', error_message
            )
            self.__acquirable_digest.rewind()
            return True  # Try again in next loop.
        self.__acquirable_digest.release()
        return True  # Try again in next loop.

    def get_defaults(self) -> Dict[str, Optional[str]]:
        """
        Return a dictionary containing any default values for parameters.
        """
        return {
            ADAPTER_KEY: "segmented",
            CONFIG_FILE_KEY: "${HOME}/digest_action.ini",
            CONFIG_SECTION_KEY: "digest_action",
            LOG_FILE_KEY: None,
            LOG_LEVEL_KEY: "INFO",
            INTERVAL_KEY: "-1",
            SAVE_FILE_KEY: "${HOME}/.digest_action",
            DEPTH_KEY: "32",
        }

    def get_digest(self, start: Optional[str]) -> ET.Element:
        """
        GETs the digest that is the basis for the next segment.
        """
        params = self.get_params()
        if None is params:
            raise DigestException("No parameters have been set")

        if None is self.__adapter:
            raise DigestException("Should not get here")
        request_url = self.__adapter.build_url(start)
        logging.debug("GET URL: %s", request_url)
        response = self.__session.get(request_url)
        status = response.status_code
        if 200 != status:
            raise DigestException(f"Failed to read Digest, status code {status}")
        return ET.fromstring(response.text)

    def get_envar_mapping(self) -> Dict[str, str]:
        """
        Return a dictionary to map environmental variables to option variables.
        """
        return {
            "DIGEST_ACTION_ADAPTER": "ADAPTER",
            "DIGEST_ACTION_CONFIG_FILE": "CONFIG_FILE",
            "DIGEST_ACTION_CONFIG_SECTION": "CONFIG_SECTION",
            "DIGEST_ACTION_LOG_FILE": "LOG_FILE",
            "DIGEST_ACTION_LOG_LEVEL": "LOG_LEVEL",
            "DIGEST_ACTION_INTERVAL": "INTERVAL",
            "DIGEST_ACTION_GET_URL": "GET_URL",
            "DIGEST_ACTION_SAVE_FILE": "SAVE_FILE",
            "DIGEST_ACTION_DEPTH": "DEPTH",
            "DIGEST_ACTION_START": "START",
        }

    def get_param_names(self) -> List[str]:
        """
        Returns a List of all the config items in which this instance is interested.
        """
        result = super().get_param_names()
        result.extend(
            [
                ADAPTER_KEY,
                GET_URL_KEY,
                SAVE_FILE_KEY,
                DEPTH_KEY,
                START_KEY,
            ]
        )
        return result

    def set_params(  # pylint: disable=useless-return
        self, params: Dict[str, Any]
    ) -> Optional[str]:
        """
        Sets a new set of parameter values.

        Returns:
            None is task configured successfully, otherwise it return
            the reason why configuration failed.
        """
        missing_option = required_options(
            [
                GET_URL_KEY,
            ],
            params,
        )
        if missing_option is not None:
            return missing_option
        resolved_path = os.path.expandvars(params[SAVE_FILE_KEY])
        if START_KEY not in params and not os.path.exists(resolved_path):
            return (
                "Start time not available as either an option,"
                + " environmental variable, or from a previous execution"
            )
        if START_KEY in params:
            self.__start = params[START_KEY]

        return super().set_params(params)


def main():
    """
    Runs a one-of execution of `digest_action`.
    """


if __name__ == "__main__":
    main()
