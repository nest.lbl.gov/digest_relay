"""
Picks a digest based on an ordinal for the digest so that the "next"
segment that is the "ordinal + 1" can be acquired after the "current"
segment is released.
"""

from typing import Callable, Optional

import os
import xml.etree.ElementTree as ET

from filelock import FileLock

from .digest_exception import DigestException
from .acquirable_digest import AcquirableDigest

_ENCODING = "utf-8"
# Wait five minutes to get start time
_WATCHER_TIMEOUT = 300


def _read_start(save_file: str) -> Optional[str]:
    """
    Read the start ordinal from its persistent store.
    """
    resolved_path = os.path.expandvars(save_file)
    lock = FileLock(f"{resolved_path}.lock", timeout=_WATCHER_TIMEOUT)
    with lock:
        if os.path.exists(resolved_path):
            with open(resolved_path, "r", encoding=_ENCODING) as opened_file:
                persisted_start = opened_file.readline()[:-1]
                return persisted_start
    return None


def _write_start(save_file: str, begin_ordinal: str) -> None:
    """
    Write the start ordinal to its persistent store.
    """
    resolved_path = os.path.expandvars(save_file)
    lock = FileLock(f"{resolved_path}.lock", timeout=_WATCHER_TIMEOUT)
    with lock:
        if None is not begin_ordinal:
            tmp_file = resolved_path + ".tmp"
            with open(tmp_file, "w", encoding=_ENCODING) as opened_file:
                persisted_start = f"{begin_ordinal}"
                opened_file.write(persisted_start + "\n")
            os.replace(tmp_file, resolved_path)


class OrdinalDigest(AcquirableDigest):
    """
    Provides access to an array of Digests returning succeeding digests
    one at a time.
    """

    def __init__(
        self,
        next_ordinal: Callable[[str], str],
        get_segment: Callable[[Optional[str]], ET.Element],
        save_file: Optional[str] = None,
        begin_ordinal: Optional[str] = None,
    ):
        self.__current: Optional[ET.Element] = None
        self.__get_segment = get_segment
        self.__save_file = save_file
        if None is not save_file:
            if None is not begin_ordinal:
                _write_start(save_file, begin_ordinal)
            self.__begin_ordinal = _read_start(save_file)
        else:
            self.__begin_ordinal = begin_ordinal
        self.__next_ordinal = next_ordinal

    def acquire(self) -> ET.Element:
        """
        Returns the next segment.
        """
        self.__current = self.__get_segment(self.__begin_ordinal)
        return self.__current

    def release(self) -> None:
        """
        Signals that current Digest has been successfully used and can
        be disposed of.
        """
        if None is self.__current:
            raise DigestException("Should not get here")
        if None is self.__begin_ordinal:
            self.__begin_ordinal = "1"
        else:
            self.__begin_ordinal = self.__next_ordinal(self.__begin_ordinal)
        if None is not self.__save_file:
            _write_start(self.__save_file, self.__begin_ordinal)

    def rewind(self) -> ET.Element:
        """
        Signals that the current Digest has not been successfully used
        so returns the original one again.
        """
        if None is self.__current:
            raise DigestException("Should not get here")
        return self.__current
