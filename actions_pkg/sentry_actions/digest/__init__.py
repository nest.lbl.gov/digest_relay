"""
This module provides a set of pre-build action that can be applied to a Digest.
"""

from .digest_action import (
    GET_URL_KEY,
    SAVE_FILE_KEY,
    DEPTH_KEY,
    START_KEY,
    ADAPTER_KEY,
    DigestAction,
)

from .digest_exception import DigestException
from .ordinal_adapter import OrdinalAdapter
from .ordinal_digest import OrdinalDigest
from .segmented_adapter import SegmentedAdapter
from .segmented_digest import SegmentedDigest
