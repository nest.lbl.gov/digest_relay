"""
An implementation of a sentry task that GETs a Digest from one URL and
then executes an action on that Digest.
"""

from typing import Callable, Optional

import xml.etree.ElementTree as ET

from .digest_adapter import DigestAdapter
from .digest_action import DEPTH_KEY, GET_URL_KEY
from .segmented_digest import SegmentedDigest


class SegmentedAdapter(DigestAdapter):
    """
    This class is used by DigestAction to handle Segmenteded digests.
    """

    def build_url(self, start: Optional[str]) -> str:
        """
        Build the URL from which to retrieve the digest.

        This default implementation build a segmenteded request, i.e. it
        includes "max" and "after" elements in the query postion of the URL.
        """
        params = self._get_params()
        if None is params:
            raise ValueError("No parameters have been set")
        query = ""
        conjunction = "?"
        selection_depth = params[DEPTH_KEY]
        if None is not selection_depth:
            query = f"{query}{conjunction}max={selection_depth}"
            conjunction = "&"
        if None is not start:
            query = f"{query}{conjunction}after={start.replace('+', '%2B')}"
            conjunction = "&"
        get_url = params[GET_URL_KEY]
        url_end = get_url.rfind("?")
        if url_end != -1:
            query = f"{query}{conjunction}{get_url[(url_end + 1):]}"
            conjunction = "&"
            effective_url = get_url[:url_end]
        else:
            effective_url = get_url
        return effective_url + query

    def create_acquirable(
        self,
        get_segment: Callable[[Optional[str]], ET.Element],
        save_file: Optional[str],
        start: Optional[str],
    ):
        """
        Creates a SegmentedDigest as the AcquirableDigest to use.
        """
        return SegmentedDigest(get_segment, save_file, start)
