"""
Provide basic access methods for a Mysql DB
"""

from typing import Any, Optional

import mysql.connector as db_api_2
from mysql.connector.errors import OperationalError

CURRENT_TIMESTAMP = "CURRENT_TIMESTAMP()"

# import psycopg2 as db_api_2
# from psycopg2 import OperationalError
# FOR MySQL
# CURRENT_TIMESTAMP="CURRENT_TIMESTAMP()"
# For postgres
# CURRENT_TIMESTAMP = "CURRENT_TIMESTAMP"


class Credentials:  # pylint: disable=too-few-public-methods
    """
    Capture DB credentials
    """

    def __init__(  # pylint: disable=too-many-arguments
        self, user: str, password: str, database: str, host: str, port: int
    ):
        """
        Creates an instance of this class
        """
        self.database = database
        self.host = host
        self.password = password
        self.port = port
        self.user = user


class BaseMysqlDB:
    """
    The class manages record keeping using a Mysql DB
    """

    def __init__(  # pylint: disable=too-many-arguments
        self, user: str, password: str, database: str, host: str, port: int
    ):
        """
        Creates an instance of this class.

        Args:
            user: The user name with which to access the DB.
            password: The password with which to access the DB.
        """
        self.__connection: Optional[Any] = None
        self.__credentials = Credentials(
            user,
            password,
            database,
            host,
            port,
        )
        self._cursor: Optional[Any] = None
        self.__require_connection = True

    def begin_transaction(self) -> bool:
        """
        Begins a transaction with the DB.

        Return:
            True if the transaction has been successfully begun, or is in progress.
        """
        if self.__require_connection:
            if None is not self.__connection:
                self.__connection.close()
            self.__connection = db_api_2.connect(
                user=self.__credentials.user,
                password=self.__credentials.password,
                database=self.__credentials.database,
                host=self.__credentials.host,
                port=self.__credentials.port,
            )
            self.__require_connection = False
        if None is not self.__connection and None is self._cursor:
            try:
                self._cursor = self.__connection.cursor(buffered=True)
                return True
            except OperationalError:
                self.__require_connection = True
                return False
        return True

    def close(self):
        """
        Closes any connections this object has opened.
        """
        self.end_transaction()
        if None is not self.__connection:
            self.__connection.close()
        self.__connection = None

    def end_transaction(self):
        """
        Ends a transaction with the DB.
        """
        self.__connection.commit()
        if None is not self._cursor:
            self._cursor.close()
        self._cursor = None
