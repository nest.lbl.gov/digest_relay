"""
An implementation of a sentry task that GETs a Digest from one URL and
then executes an action on that Digest.
"""

from typing import Any, Dict, List, Optional

import argparse

from sentry import (
    Config,
    Task,
    COMMAND_KEY,
    CONFIG_FILE_KEY,
    CONFIG_SECTION_KEY,
    LOG_FILE_KEY,
    LOG_LEVEL_KEY,
    LOG_LEVELS,
    PID_FILE_KEY,
)

INTERVAL_KEY = "interval"


class ActionTask(Task):
    """
    This class GETs a Digest from one URL and then executes an action on
    that Digest.
    """

    def __init__(self) -> None:
        """
        Creates an instance of this class.
        """
        self.__params: Optional[Dict[str, Any]] = None

    def create_argument_parser(
        self, include_interval: bool = True
    ) -> argparse.ArgumentParser:
        """
        Creates and populated the argparse.ArgumentParser for this executable.
        """
        defaults = self.get_defaults()
        parser = argparse.ArgumentParser(
            description=f"Executes an instance of the {type(self).__name__} class"
        )
        parser.add_argument(
            "-i",
            f"--{CONFIG_FILE_KEY}",
            dest="CONFIG_FILE",
            help='The path to the configuration file, the default is "'
            + str(defaults[CONFIG_FILE_KEY])
            + '"',
        )
        parser.add_argument(
            "-s",
            f"--{CONFIG_SECTION_KEY}",
            dest="CONFIG_SECTION",
            help="The section of the INI file to use for this execution,"
            + ' the default is "'
            + str(defaults[CONFIG_SECTION_KEY])
            + '"',
        )
        parser.add_argument(
            f"--{LOG_FILE_KEY}",
            dest="LOG_FILE",
            help="The file, as opposed to stdout, into which to write log messages",
        )
        parser.add_argument(
            "-l",
            f"--{LOG_LEVEL_KEY}",
            dest="LOG_LEVEL",
            help="The logging level for this execution",
            choices=LOG_LEVELS.keys(),
        )
        parser.add_argument(
            "-c",
            f"--{COMMAND_KEY}",
            dest="COMMAND",
            help="The command to execute on the running instance of this service",
        )
        parser.add_argument(
            "-p",
            f"--{PID_FILE_KEY}",
            dest="PID_FILE",
            help="The path in which to store the executing pid",
        )
        if include_interval:
            parser.add_argument(
                "-t",
                f"--{INTERVAL_KEY}",
                dest="INTERVAL",
                help="The number of seconds between update executions",
            )
        return parser

    def get_command_key(self) -> Optional[str]:
        """
        Returns the config item containing the name of requested command.
        """
        return COMMAND_KEY

    def get_config_file_key(self) -> Optional[str]:
        """
        Returns the name of the config file to use, if any.
        """
        return CONFIG_FILE_KEY

    def get_config_section_key(self) -> Optional[str]:
        """
        Returns the section of the config file to use, if any.
        """
        return CONFIG_SECTION_KEY

    def get_defaults(self) -> Dict[str, Optional[str]]:
        """
        Return a dictionary containing any default values for parameters.
        """
        return {
            CONFIG_FILE_KEY: "${HOME}/sentry_action.ini",
            CONFIG_SECTION_KEY: "sentry_action",
            LOG_FILE_KEY: None,
            LOG_LEVEL_KEY: "INFO",
            INTERVAL_KEY: "-1",
        }

    def get_envar_mapping(self) -> Dict[str, str]:
        """
        Return a dictionary to map environmental variables to option variables.
        """
        return {
            "ACTION_CONFIG_FILE": "CONFIG_FILE",
            "ACTION_CONFIG_SECTION": "CONFIG_SECTION",
            "ACTION_LOG_FILE": "LOG_FILE",
            "ACTION_LOG_LEVEL": "LOG_LEVEL",
            "ACTION_INTERVAL": "INTERVAL",
        }

    def get_interval(self) -> Optional[float]:
        """
        Returns the number of seconds to wait between executions.
        """
        if None is self.__params:
            return None
        return self.__params[INTERVAL_KEY]  # pylint: disable=unsubscriptable-object

    def get_log_file_key(self) -> Optional[str]:
        """
        Returns the name of the log file to use, if any.
        """
        return LOG_FILE_KEY

    def get_log_level_key(self) -> Optional[str]:
        """
        Returns the name of the log level to use, if any.
        """
        return LOG_LEVEL_KEY

    def get_param_names(self) -> List[str]:
        """
        Returns a List of all the config items in which this instance is interested.
        """
        return [
            COMMAND_KEY,
            LOG_FILE_KEY,
            LOG_LEVEL_KEY,
            PID_FILE_KEY,
            INTERVAL_KEY,
        ]

    def get_params(self) -> Optional[Dict[str, Any]]:
        """
        Returns the dictionary contains all of the parameters for this task..
        """
        return self.__params

    def get_pid_file_key(self) -> Optional[str]:
        """
        Returns config item containing the name of the pid file to use.
        """
        return PID_FILE_KEY

    def set_params(  # pylint: disable=useless-return
        self, params: Dict[str, Any]
    ) -> Optional[str]:
        """
        Sets a new set of parameter values.

        Returns:
            None is task configured successfully, otherwise it return
            the reason why configuration failed.
        """
        self.__params = params
        return None

    def updated_configuration(self, _: Config) -> None:
        """
        Informs this instance of the contents of the current configuration file. This can be used to
        construct the return value of 'get_param_names'.
        """
        self.__params = None


def main():
    """
    Runs a one-of execution of this class.
    """


if __name__ == "__main__":
    main()
