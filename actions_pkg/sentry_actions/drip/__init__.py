"""
This module provides the classes and function to drip feed files from
one directory to another.
"""

from .dropper import Dropper as Dropper  # pylint: disable=useless-import-alias
from .file_dropper import (  # pylint: disable=useless-import-alias
    FileDropper as FileDropper,
)
