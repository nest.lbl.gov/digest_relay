"""
This shows how to create an executeable to run a Task using the
sentry.service module.
"""

from sentry import IntervalService


def main() -> int:
    """
    Runs the nop_task as a sentry.
    """
    service = IntervalService("drip_task")
    return service.run()


if __name__ == "__main__":
    main()
