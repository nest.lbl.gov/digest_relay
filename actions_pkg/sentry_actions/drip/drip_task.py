"""
An implementation of a sentry Task that runs a "drip system".
"""

from typing import Any, Dict, List, Optional, Type

import argparse
from importlib.metadata import entry_points
import logging

from sentry_actions import (
    ActionTask,
    COMMAND_KEY,
    CONFIG_FILE_KEY,
    CONFIG_SECTION_KEY,
    INTERVAL_KEY,
    LOG_FILE_KEY,
    LOG_LEVEL_KEY,
    required_options,
    required_positionals,
)

from .dropper import Dropper
from .faucet import Faucet

_HEADERS = {"Content-Type": "application/xml", "Accept": "application/xml"}

DROPPER_CLASS_KEY = "dropper_class"
DROP_INTERVAL_KEY = "drop_interval"
FULL_INTERVAL_KEY = "full_interval"
REFILL_INTERVAL_KEY = "refill_interval"
BATCH_SIZE_KEY = "batch_size"

DRIP_SERVICE_GROUP = "drip.dropper"


def _select_plugin_class(name) -> Type[Dropper]:
    """
    Returns the selected drop.Dropper class object.
    """
    droppers = entry_points(group=DRIP_SERVICE_GROUP)
    known_names = []
    logging.debug("Begin known Droppers:")
    for entry in droppers:
        if entry.name not in known_names:
            logging.debug("    %s", entry)
            known_names.append(entry.name)
    logging.debug("End known Droppers:")
    for entry in droppers:
        if name == entry.name:
            return entry.load()
    raise ValueError(f'No known Dropper implementation named "{name}"')


class DripTask(ActionTask):
    """
    This class runs a "drip system". In this case a "drip" is made up of
    a sequence of drops.
    """

    def __init__(self) -> None:
        """
        Creates an instance of this class.
        """
        super().__init__()
        self.__faucet: Optional[Faucet] = None

    def create_argument_parser(
        self, include_interval: bool = False
    ) -> argparse.ArgumentParser:
        """
        Creates and populated the argparse.ArgumentParser for this executable.
        """
        parser = super().create_argument_parser(include_interval=False)
        parser.add_argument(
            "-d",
            f"--{DROP_INTERVAL_KEY}",
            dest="DROP_INTERVAL",
            help="The number of seconds to wait when the destination still has space,"
            + " before checking again",
        )
        parser.add_argument(
            "-f",
            f"--{FULL_INTERVAL_KEY}",
            dest="FULL_INTERVAL",
            help="The number of seconds to wait when the destination is full,"
            + " before checking again",
        )
        parser.add_argument(
            "-r",
            f"--{REFILL_INTERVAL_KEY}",
            dest="REFILL_INTERVAL",
            help="The number of seconds to wait after the source is empty before checking again.",
        )
        parser.add_argument(
            "-b",
            f"--{BATCH_SIZE_KEY}",
            dest="BATCH_SIZE",
            help="The number of dropped items in a drip",
        )
        parser.add_argument(
            dest="DROPPER_CLASS",
            nargs="?",
            help="The package:Class defining to drip:Dropper subclass to execute",
        )
        return parser

    def execute(self) -> bool:
        """
        Executes the responsibilities of this executable
        """
        self.__get_faucet().release_drop()
        return True

    def get_defaults(self) -> Dict[str, Optional[str]]:
        """
        Return a dictionary containing any default values for parameters.
        """
        return {
            CONFIG_FILE_KEY: "${HOME}/drip-drip.ini",
            CONFIG_SECTION_KEY: "drip_drip",
            LOG_FILE_KEY: None,
            LOG_LEVEL_KEY: "INFO",
            INTERVAL_KEY: "-1",
            DROP_INTERVAL_KEY: "10",
            FULL_INTERVAL_KEY: "30",
            REFILL_INTERVAL_KEY: "120",
            BATCH_SIZE_KEY: "3",
        }

    def get_envar_mapping(self) -> Dict[str, str]:
        """
        Return a dictionary to map environmental variables to option variables.
        """
        return {
            "DRIP_DROPPER_CLASS": "DROPPER_CLASS",
            "DRIP_CONFIG_FILE": "CONFIG_FILE",
            "DRIP_CONFIG_SECTION": "CONFIG_SECTION",
            "DRIP_LOG_FILE": "LOG_FILE",
            "DRIP_LOG_LEVEL": "LOG_LEVEL",
            "DRIP_DROP_INTERVAL": "DROP_INTERVAL",
            "DRIP_FULL_INTERVAL": "FULL_INTERVAL",
            "DRIP_REFILL_INTERVAL": "REFILL_INTERVAL",
            "DRIP_BATCH_SIZE": "BATCH_SIZE",
        }

    def __get_faucet(self) -> Faucet:  # pylint: disable=too-many-branches
        if None is self.__faucet:
            params = self.get_params()
            if None is params:
                raise ValueError("No parameters have been set")

            # create new faucet
            dropper_name = params[DROPPER_CLASS_KEY]
            if None is dropper_name:
                raise ValueError(
                    "The name of the Dropper plug-in to use has not been specified"
                )
            dropper_class = _select_plugin_class(dropper_name)
            if CONFIG_SECTION_KEY not in params:
                if CONFIG_SECTION_KEY in params:
                    dropper = dropper_class(params[CONFIG_FILE_KEY])
                else:
                    dropper = dropper_class()
            else:
                dropper = dropper_class(
                    params[CONFIG_FILE_KEY], params[CONFIG_SECTION_KEY]
                )
            logging.info("Drip feeding using the %s class", type(dropper).__name__)

            if FULL_INTERVAL_KEY in params:
                full_interval = float(params[FULL_INTERVAL_KEY])
            else:
                full_interval = None
            if REFILL_INTERVAL_KEY in params:
                refill_interval = float(params[REFILL_INTERVAL_KEY])
            else:
                refill_interval = None
            self.__faucet = Faucet(
                dropper,
                drop_size=int(params[BATCH_SIZE_KEY]),
                drop_interval=float(params[DROP_INTERVAL_KEY]),
                full_interval=full_interval,
                refill_interval=refill_interval,
            )
        return self.__faucet

    def get_interval(self) -> Optional[float]:
        """
        Returns the number of seconds to wait between executions.
        """
        return self.__get_faucet().get_interval()

    def get_param_names(self) -> List[str]:
        """
        Returns a List of all the config items in which this instance is interested.
        """
        result = super().get_param_names()
        result.extend(
            [
                DROPPER_CLASS_KEY,
                CONFIG_FILE_KEY,
                CONFIG_SECTION_KEY,
                DROP_INTERVAL_KEY,
                FULL_INTERVAL_KEY,
                REFILL_INTERVAL_KEY,
                BATCH_SIZE_KEY,
            ]
        )
        return result

    def set_params(  # pylint: disable=useless-return
        self, params: Dict[str, Any]
    ) -> Optional[str]:
        """
        Sets a new set of parameter values.

        Returns:
            None is task configured successfully, otherwise it return
            the reason why configuration failed.
        """
        if COMMAND_KEY not in params:
            missing_option = required_options(
                [
                    BATCH_SIZE_KEY,
                    DROP_INTERVAL_KEY,
                ],
                params,
            )
            if missing_option is not None:
                return missing_option
            missing_positional = required_positionals(
                [
                    DROPPER_CLASS_KEY,
                ],
                params,
            )
            if missing_positional is not None:
                return missing_positional
        self.__faucet = None
        return super().set_params(params)
